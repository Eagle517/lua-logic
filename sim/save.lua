
logicsavecallbacks = {}

function logicsavecallback(id, cb)
	logicsavecallbacks[id] = cb
end

function logicsave()
	for cbid, cb in pairs(logicsavecallbacks) do
		cb()
	end
end
