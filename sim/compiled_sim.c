
void sim_init(int num_gates, int num_nets);
void sim_add_gate();
void sim_add_net();
void sim_tick();
void sim_get_net_state(int objref);
void sim_get_port_state(int objref, int index);
