
assert(not io.safe, "re-execution of iosafe")

local iosafe = io
_G.io = {safe = true}
local requiresafe = require
_G.require = nil
local ossafe = os
_G.os = {safe = true}

local savedir = OPT_SAVE_DIR

function io.open(fn, mode)
	fn = fn:gsub("\\", "/")
	assert(not fn:find("[^a-zA-Z0-9%._ ]"), "illegal character in file name \""..fn.."\"")
	assert(not fn:find("%.%."), "illegal updir in file name \""..fn.."\"")
	fn = fn:gsub("^/", "")
	
	fn = savedir.."/"..fn
	
	print("access file \""..fn.."\"")
	
	return iosafe.open(fn, mode)
end

os.clock = ossafe.clock
