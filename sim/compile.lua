
local ffi = FFI or require("ffi")
Simulation = Simulation or {}

ffi.cdef[[
	
]]

function Simulation.compile(sim)
	-- assemble a list of all nets
	local all_nets = {}
	local all_nets_t = {}
	for k, wire in pairs(sim.wires) do
		local net = Wire.getgroup(wire)
		all_nets_t[net] = net
	end
	local num_nets = 0
	for net_id, net in pairs(all_nets_t) do
		table.insert(all_nets, net)
	end
	
	-- assemble a list of all gates
	local all_gates = {}
	for k, gate in pairs(sim.gates) do
		table.insert(all_gates, gate)
	end
	
	-- construct each gate into an array
	
	-- construct array of all nets
	local c_nets = ffi.new("struct Net["..(#all_nets).."]")
	for net_idx, net in ipairs(all_nets) do
		local c_net = ffi.new("struct Net", #net.gates_update)
		
		for gate_idx, gate in ipairs(net.gates_update) do
			
		end
		
		c_nets[net_idx] = c_net
	end
end

function Simulation.decompile(sim)
	
end

function Simulation.tick_compiled(sim)
	
end
