
Wire = {}

function Wire.new(objref, layer, bounds)
	local o = {
		objref = objref,
		layer = layer,
		group = nil,
		bounds = bounds,
	}
	return o
end

function Wire.setlayer(wire, layer)
	if wire.group ~= nil then
		Group.removewire(wire.group, wire)
	end
	wire.layer = layer
	Simulation.connectwire(GSim, wire)
end

function Wire.update(wire)
	network_send("WU\t" .. (wire.group.state~=0 and "1" or "0") .. "\t" .. wire.objref .. "\n")
end

function Wire.setgroup(wire, group)
	wire.group = group
end

function Wire.getgroup(wire)
	return wire.group
end

function Wire.getobjref(wire)
	return wire.objref
end

function Wire.getlayer(wire)
	return wire.layer
end

function Wire.getbounds(wire)
	return wire.bounds
end
