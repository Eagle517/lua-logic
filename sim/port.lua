
local ffi = FFI

PortTypes = {
	output = 0,
	input = 1,
}

PortDirections = {
	[0] = {-1, 0, 0},
	[1] = {0, 1, 0},
	[2] = {1, 0, 0},
	[3] = {0, -1, 0},
	[4] = {0, 0, 1},
	[5] = {0, 0, -1}
}

Port = {}

function Port.new(type, direction, position, causeupdate, idx)
	local o = {
		--state = ffi.new("long long", 0),
		state = 0,
		group = nil,
		
		type = type,
		direction = direction,
		position = position,
		causeupdate = causeupdate,
		gate = nil,
		idx = idx,
	}
	return o
end

function Port.getconnectionposition(port)
	local offset = PortDirections[port.direction]
	return {port.position[1]+offset[1], port.position[2]+offset[2], port.position[3]+offset[3]}
end

function Port.isrising(port)
	return port.group.state==1 and (port.group.update_tick == GSim.current_tick)
end

function Port.isfalling(port)
	return port.group.state==0 and (port.group.update_tick == GSim.current_tick)
end

function Port.getgate(port)
	return port.gate
end

function Port.setgate(port, gate)
	port.gate = gate
end

function Port.setgroup(port, group)
	port.group = group
	Port.getgate(port).port_nets[port.idx] = group
end

function Port.getgroup(port)
	return port.group
end

function Port.gettype(port)
	return port.type
end

function Port.getstate(port)
	return port.state
end
